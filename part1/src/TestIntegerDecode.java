import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

public class TestIntegerDecode {

    private void TestTakeException(String string){
        Assertions.assertThrows(NumberFormatException.class, () -> Integer.decode(string));
    }

    private void TestTakeInteger(String string, Integer expected){
        Assertions.assertEquals(expected,Integer.decode(string));
    }

    @Test
    public void TestAllExceptions(){
        TestTakeException("");
        TestTakeException("456qwer");
        TestTakeException("masters");
        TestTakeException("0xQWERTY");
        TestTakeException("08");
        TestTakeException("678-91");
        TestTakeException("56+34");
        TestTakeException("#45qwer");
        TestTakeException("-2147483649");
        TestTakeException("+2147483648");
    }
    @Test
    public void TestZero(){
        TestTakeInteger("-0",0);
        TestTakeInteger("+0",0);
        TestTakeInteger("0",0);
        TestTakeInteger("0x0",0);
        TestTakeInteger("0X0",0);
        TestTakeInteger("00",0);
        TestTakeInteger("#0",0);

    }

    @Test
    public void TestInteger(){
        TestTakeInteger("-228",-228);
        TestTakeInteger("322",322);
        TestTakeInteger(String.valueOf(Integer.MAX_VALUE),Integer.MAX_VALUE);
        TestTakeInteger(String.valueOf(Integer.MIN_VALUE),Integer.MIN_VALUE);
        TestTakeInteger("022",18);
        TestTakeInteger("#FF",255);
        TestTakeInteger("0xFF",255);
        TestTakeInteger("0XFF",255);
    }


}
